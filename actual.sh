#!/bin/bash

RUTA_PRINCIPAL="/home/$USER/scripts/brillo"
RUTA_BRILLO="$RUTA_PRINCIPAL/brillo.txt" #Busca la ruta del valor de brillo
RUTA_HORARIO="$RUTA_PRINCIPAL/horario.txt"
RUTA_GAMA="$RUTA_PRINCIPAL/gama.txt" #Busca la ruta de los valores de la gama de colores

gama=1:1:1 #gama de colores por defecto ROJO:VERDE:AZUL

HORA=`date +%H%M`
NOCHE=`awk 'NR==1' $RUTA_HORARIO`
DIA=`awk 'NR==2' $RUTA_HORARIO`

brillo=`awk 'NR==1' $RUTA_BRILLO` #Lee la primera linea asignada al brillo

if ! [ $HORA -ge $DIA ] || ! [ $HORA -lt $NOCHE ]; then #Si Hora no es Mayor igual al Dia u Hora no es menor a la Noche

	gama=`awk 'NR==1' $RUTA_GAMA`

fi

xrandr --output LVDS1 --brightness $brillo --gamma $gama #$ROJO:$VERDE:$AZUL #Aplica el brillo

notify-send "Brillo: `calc $brillo*100`" -t 1000
