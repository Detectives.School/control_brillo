#!/bin/bash

TRUE=1

RUTA_PRINCIPAL="/home/$USER/scripts/brillo"
RUTA_BRILLO="$RUTA_PRINCIPAL/brillo.txt" #Busca la ruta del valor de brillo
RUTA_HORARIO="$RUTA_PRINCIPAL/horario.txt"
RUTA_GAMA="$RUTA_PRINCIPAL/gama.txt" #Busca la ruta de los valores de la gama de colores


UNIDAD=0.1 #Unidad de brillo
MAXIMO=1 #Valor maximo de brillo
gama=1:1:1 #gama de colores por defecto ROJO:VERDE:AZUL

HORA=`date +%H%M`
NOCHE=`awk 'NR==1' $RUTA_HORARIO`
DIA=`awk 'NR==2' $RUTA_HORARIO`

brillo=`awk 'NR==1' $RUTA_BRILLO` #Lee la primera linea asignada al brillo

ES_MENOR_AL_MAXIMO=$( echo "$brillo<$MAXIMO" | bc )

if [ $ES_MENOR_AL_MAXIMO -eq $TRUE ]; then

	brillo=`calc $brillo + $UNIDAD` #Suma el valor de brillo
	
	if  ! [ $HORA -ge $DIA ] || ! [ $HORA -lt $NOCHE ]; then #Si Hora no es Mayor igual al Dia u Hora no es menor a la Noche

		gama=`awk 'NR==1' $RUTA_GAMA`

	fi

	xrandr --output LVDS1 --brightness $brillo --gamma $gama #Aplica el brillo
	echo $brillo > $RUTA_BRILLO #Guarda el valor de brillo

fi

notify-send "Brillo: `calc $brillo*100`" -t 1000
